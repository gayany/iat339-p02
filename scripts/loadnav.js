//LOAD NAV
document.getElementById('nav').innerHTML = '<nav class="nav"><a class="brandlogo tools desktop" href="index.html">Uniji</a><input type="checkbox" id="sandwich_toggle"><label class="mobile" for="sandwich_toggle" id="sandwich_toggler"><i class="fas fa-bars dropdown"></i><i class="fas fa-times close"></i></label><a href="cart.html" class="mobile cart"><i class="fas fa-shopping-cart"></i></a><ul class="nav_select"><li class="page mobile"><a href="index.html">Home</a></li><li class="page"><a href="about.html">About</a></li><li class="page"><a href="shop.html">Shop</a></li><li class="page"><a href="lookbook.html">Lookbook</a></li><li class="page mobile"><a href="wishlist.html">Wishlist</a></li><li><a href="wishlist.html" class="tools desktop"><i class="fas fa-heart desktop"></i></a></li><li><li><a href="cart.html" class="tools desktop"><i class="fas fa-shopping-cart tools"></i></a></li></ul></nav>'
	
/**

'<nav class="nav"><a class="brandlogo tools desktop" href="home.html">Uniji</a><input type="checkbox" id="sandwich_toggle"><label class="mobile" for="sandwich_toggle" id="sandwich_toggler" ><i class="fas fa-bars dropdown"></i><i class="fas fa-times close"></i></label><a href="cart.html" class="mobile cart"><i class="fas fa-shopping-cart"></i></a><ul class="nav_select"><li class="page"><a href="about.html">About</a></li><li class="page"><a href="shop.html">Shop</a></li><li class="page"><a href="shop.html">Lookbook</a></li><li><a href="#" class="tools desktop"><i class="fas fa-heart desktop"></i></a></li><li><li><a href="cart.html" class="tools desktop"><i class="fas fa-shopping-cart tools"></i></a></li></ul></nav>';

**/

/**
	'<ul class="nav"><li class="brandlogo"><a href="home.html">Uniji</a></li><li class="nav_select"></li><li><a href="cart.html"><i class="fas fa-shopping-cart tools nav_select"></i></a></li><li><a href="#"><i class="fas fa-heart tools nav_select"></i></a></li><li><a href="shop.html" class="nav_select">Shop</a></li><li><a href="#" class="nav_select">Lookbook</a></li><li><a href="about.html" class="nav_select">About</a></li></ul>'
**/


//LOAD FOOTER
document.getElementById('footer').innerHTML = '<footer><div class="container"><div class="col col_clear"><a href="./about.html#contact">Find a Store</a><br><a href="./index.html#newsletter">Join Our Newsletter</a><br><a href="./about.html#contact">Send Feedback</a><br><a href="styleguide.html">Style Guide</a></div><div class="col col_clear"><p>100 City Centre Dr,<br> Mississauga, ON<br>L5B 2C9<br>519-592-0319</p></div><div class="col social col_clear"><a><i class="fab fa-facebook"></i></a><a><i class="fab fa-instagram"></i></a><a><i class="fab fa-twitter"></i></a><br><a href="citations.html">Citations</a><br><a href="https://gitlab.com/gayany/iat339-p02">Gitlab</a></div><p class="copyright">Canada © 2019 Uniji, Inc. All Rights Reserved</p></div></footer>'

