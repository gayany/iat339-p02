/** LIST OF PRODUCTS **/
var products = [];

/** PRODUCTS **/
//0
products.push({
	brand: "Darlyyshop",
	product_name: "Baggy Yarn Cardigan",
	price: "40.00",
	color: ["Beige", "White", "Brown"],
	details: "Inspired by a much-loved piece from the Darlyyshop capsule, the cardigan is knit with a soft alpaca-blend yarn from Italy. The result? Ultimate coziness.",
	features: "Spandex-plaited hem and cuffs for shape retention",
	materials: "Origin: Yarn from Italy<br>Content: 72% alpaca, 15% polyamide, 13% wool<br>Care: Hand wash<br>Imported"
});
//1
products.push({
	brand: "Cherrykoko",
	product_name: "Vivienne Dress",
	price: "30.00",
	color: ["Black"],
	details: "This is the one. This version of Cherrykoko's signature minimal camisole dress is cut from a fluid fabric that drapes beautifully.",
	features: "Adjustable straps<br>Fully lined",
	materials: "Content: 100% rayon; Lining: 100% viscose<br>Care: Dry clean<br>Imported"
});
//2
products.push({
	brand: "Sonyunara",
	product_name: "Floral Silk Dress",
	price: "35.00",
	color: ["White", "Black"],
	details: "Your go-to casual dress. This is a light-weight dress with a tie collar and voluminous lantern sleeves. It's made with 100% silk that's been sandwashed for a soft, sueded look.",
	features: "Tie collar<br>Partially lined<br>Loop-and-button closure at cuff ",
	materials: "Content: 100% silk; Lining: 100% polyester<br>Care: Dry clean<br>Imported"
});
//3
products.push({
	brand: "Evisu",
	product_name: "Matte Satin Blouse",
	price: "25.00",
	color: ["Pink", "Purple"],
	details: "Cut from silky-smooth matte satin, this Babaton favourite is a streamlined take on the classic dress shirt. The minimalist shawl collar highlights the fabric's beautiful drape.",
	features: "Hook-and-thread closure at top of placket",
	materials: "Content: 100% polyester<br>Care: Machine wash<br>Imported"
});
//4
products.push({
	brand: "Gaennso",
	product_name: "Wool Coat with Hood",
	price: "99.00",
	color: ["Gray", "Beige"],
	details: "Crafted from an elevated new fabric chosen for its superior quality and texture, this version of the Stedman is beautifully warm. It's tailored with a unique double-faced wool blend from a premier Italian mill, and constructed to have a luxurious, contemporary finish.",
	features: "Single zipper closure<br>Partially lined<br>Inseam pockets",
	materials: "Origin: Yarn from Italy<br>Content: 96% wool, 4% nylon; Lining: 100% cupro<br>Care: Dry clean<br>Imported"
});

products.push({
	brand: "Cecil McBee",
	product_name: "Cropped Slim-fit Dress Pants",
	price: "35.00",
	color: ["White", "Gray", "Black"],
	details: "Just like the iconic Cohen Pant, but without the pleats. These ones are made with Terado™, a Japanese fabric that's matte crepe on one side and satin on the other.",
	features: "Faux fly detail<br>Slash pockets<br>Pull-on waistband with elastic back",
	materials: "Origin: Fabric from Japan<br>Content: 82% triacetate, 18% polyester; Pocket lining: 100% cupro<br>Care: Dry clean<br>Imported"
});

products.push({
	brand: "Shimamura",
	product_name: "High-waisted Skinny Jeans",
	price: "30.00",
	color: ["White", "Black", "Gray"],
	details: "Cut for a second-skin fit, these jeans are made with premium stretch denim that holds its shape. The ultra-high rise does exactly what you think it does: makes your legs look crazy long.",
	features: "Five-pocket styling",
	materials: "Designer Wash Name: Black Galaxy <br>Content: 85% cotton, 9% elastomultiester, 6% elastane <br>Care: Machine wash<br>Imported"
});

products.push({
	brand: "Nibbuns",
	product_name: "Relax Stitched Sweater",
	price: "34.00",
	color: ["Navy", "Black", "Red", "Green"],
	details: "This is a relaxed-fit sweater with a half-cardigan stitch for texture. It's made with a fine-gauge yarn that's lightweight yet warm.",
	features: "Spandex-tipped hem and cuffs for shape retention",
	materials: "Content: 60% rayon, 40% nylon<br>Care: Hand wash<br>Imported"
});


console.log("PRODUCTS: " + products.length);
var current = '1';

/** SHOP -> PRODUCT PAGE SAVE ID & GET ID**/
function saveID(imgID) {
	console.log(imgID + " SAVED.")
	window.localStorage.setItem("ID", imgID);
}

/** LOAD PRODUCT PAGE**/
function loadProduct() {
	current = localStorage.getItem("ID");
	console.log("id loaded: " + current);

}

/** ADDING TO CART & WISHLIST **/

var wishlist;
var cart;

function getSaved() {
	if (JSON.parse(localStorage.getItem('cart') != undefined || JSON.parse(localStorage.getItem('cart') == null || JSON.parse(localStorage.getItem('cart') == 0)))) {
		cart = JSON.parse(localStorage.getItem('cart'));
	}

	if (cart == null || cart == undefined || cart.length == 0) {
		console.log("New Cart Arr Created.");
		cart = [];
	}

	console.log("Cart Storage Length: " + cart.length);
	loadProduct();

	if (JSON.parse(localStorage.getItem('wishlist') != undefined || JSON.parse(localStorage.getItem('wishlist') == null || JSON.parse(localStorage.getItem('wishlist') == 0)))) {
		wishlist = JSON.parse(localStorage.getItem('wishlist'));
	}

	if (wishlist == null || wishlist == undefined || wishlist.length == 0) {
		console.log("New Wishlist Arr Created.");
		wishlist = [];
	}
}

function addCart() {
	console.log("CART ITEMS: " + cart.length);
	cart.push(current);

	localStorage.setItem("cart", JSON.stringify(cart));
	console.log(current + " added to cart." + "Cart Items: " + cart.length);
}

function loadPopup() {
	/** ADDED TO CART POP UP **/
	document.getElementById("cart_popup").innerHTML = '<h3 class="title">Added to cart!</h3>' +
		'<div ' + 'id=' + '"product' + current + '" class="item row_nowrap">' +
		'<div class="col col2">' +
		'<a href="product.html" onmousedown="saveID(\'' + current + '\')"><img class="mobile_img" src="imgs/product' + current + '/1.png"></a></div>' +
		'<div class="col col2">' + '<h5 class="brand">' + products[current].brand + '</h5>' +
		'<div class="row"><a class="col21_1 product_name" href="product.html" onmousedown="saveID(\'' + current + '\')"><h4>' + products[current].product_name + '</h4></a>' +
		'</div>' +
		'<div class="col options"><p>Color: Black</p><p>Size: Medium</p></div> ' + '<p class="price">$' + products[current].price + '</p>' +
		'</div></div><a class="default_button cart_button" href="cart.html">View Cart</a>';
}

function addWishlist(imgID) {

//	if (window.location.pathname.indexOf('/product.html') > -1) {
//		console.log("Product Wishlist");
//		if (wishlist.indexOf(current) <= -1) {
//			wishlist.push(current);
//			console.log(current + " added to wishlist." + "Wishlist Items: " + wishlist.length);
//		}
//	} else {
//		console.log("Other Wishlist");
//		if (wishlist.indexOf(imgID) <= -1) {
//			wishlist.push(imgID);
//			console.log(imgID + " added to wishlist." + "Wishlist Items: " + wishlist.length);
//		}
//	}
	
				wishlist.push(imgID);
			console.log(imgID + " added to wishlist." + "Wishlist Items: " + wishlist.length);
	localStorage.setItem("wishlist", JSON.stringify(wishlist));
}

function removeWishlist(imgID) {
	console.log("Wishlist size: " + wishlist.length);

	if (wishlist.indexOf(imgID) > -1) {
		wishlist.splice(wishlist.indexOf(imgID), 1);
		console.log(imgID + " removed from Wishlist.");
		console.log("Wishlist size: " + wishlist.length);
		localStorage.setItem("wishlist", JSON.stringify(wishlist));
		loadWishlist();
	}
}

function removeCart(imgID) {
	console.log("Cart size: " + cart.length);

	if (cart.indexOf(imgID) > -1) {
		cart.splice(cart.indexOf(imgID), 1);
		console.log(imgID + " removed from Cart.");
		console.log("Cart size: " + cart.length);
		localStorage.setItem("cart", JSON.stringify(cart));
		loadCart();
	}
}

window.onload = function () {
	console.log("Window Loaded");

	getSaved();

	console.log("Wishlist Storage Length: " + wishlist.length);
	loadProduct();

	/** On Product Page **/
	if (window.location.pathname.indexOf('/product.html') > -1) {
		loadProducts();
	}
	/** ON CART PAGE **/
	if (window.location.pathname.indexOf('/cart.html') > -1) {
		loadCart();
	}
	/** ON WISHLIST PAGE **/
	if (window.location.pathname.indexOf('/wishlist.html') > -1) {
		loadWishlist();
	}

	/** LOAD PRODUCT LISTS **/
	if (document.getElementsByClassName("product_list") != null) {
		console.log("Loading product list");
		var i;
		for (i = 0; i < 8; i++) {
			// console.log("Product id: product"+i);
			//console.log(products[i].brand);
			var t1 = document.getElementById("product" + i);
			console.log("t1: " + t1);
			console.log("product" + i);
			if (t1 != null || t1 != undefined) {
				t1.getElementsByClassName("brand")[0].textContent = products[i].brand;
				t1.getElementsByClassName("product_title")[0].textContent = products[i].product_name;
				t1.getElementsByClassName("price")[0].textContent = "$" + products[i].price;
			}
		}
	}
}

function loadWishlist() {
	console.log("On wishlist page");
	var i;
	for (i = 0; i < wishlist.length; i++) {
		document.getElementById("cart").innerHTML = document.getElementById("cart").innerHTML +
			'<div ' + 'id=' + '"product' + i + '" class="item row_nowrap">' +
			'<div class="col col2">' +
			'<a href="product.html" onmousedown="saveID(\'' + wishlist[i] + '\')"><img class="mobile_img" src="imgs/product' + wishlist[i] + '/1.png"></a></div>' +
			'<div class="col col2">' + '<h5 class="brand">' + products[wishlist[i]].brand + '</h5>' +
			'<div class="row"><a class="col21_1 product_name" href="product.html" onmousedown="saveID(\'' + wishlist[i] + '\')"><h4>' + products[wishlist[i]].product_name + '</h4></a>' +
			'<p class="col21_2" class="price">$' + products[wishlist[i]].price + '</p></div>' +
			'<div class="col options"><p>Color: Black</p><p>Size: Medium</p></div>' +
			'<a class="wishlist default_button remove" onclick="removeWishlist(\'' + wishlist[i] + '\');location.reload();">delete</a></div>' +
			'</div>';

	}
}

function loadProducts() {
	console.log("On product page");

	/** PRODUCT DETAILS **/
	if (document.getElementById("brand") != null) {
		document.getElementById("brand").innerHTML = products[current].brand;
	}
	if (document.getElementById("product_name") != null) {
		document.getElementById("product_name").innerHTML = products[current].product_name;
	}

	if (document.getElementById("price") != null) {
		document.getElementById("price").innerHTML = "$" + products[current].price;
	}

	if (document.getElementById("color") != null) {
		var i;
		for (i = 0; i < products[current].color.length; i++) {

			document.getElementById("color").innerHTML = document.getElementById("color").innerHTML + "<option>" + products[current].color[i] + "</option";
		}
	}

	if (document.getElementById("details") != null) {
		document.getElementById("details").innerHTML = products[current].details;
	}

	if (document.getElementById("features") != null) {
		document.getElementById("features").innerHTML = products[current].features;
	}

	if (document.getElementById("materials") != null) {
		document.getElementById("materials").innerHTML = products[current].materials;
	}
	
	if (document.getElementById("wishlist_button") != null) {
		document.getElementById("wishlist_button").onClick = function() { addWishlist(current); };
	}
	
	/** PRODUCT BANNER **/
	document.getElementById("banner").src = "./imgs/product" + current + "/0.png";

	/** PRODUCT CAROUSEL **/
	var inner;
	var i;
	for (i = 1; i < 10; i++) {
		if (document.getElementById("img" + i) != null) {
			document.getElementById("img" + i).src = "imgs/product" + current + "/" + i + ".png";
		} else {
			console.log("img not found.");
			break;
		}
	}
}

function loadCart() {
	/** On cart page **/
	console.log("On cart/wishlist page");
	var subtotal = 0;
	/** PRODUCT DETAILS **/
	var i;
	for (i = 0; i < cart.length; i++) {
		document.getElementById("cart").innerHTML = document.getElementById("cart").innerHTML +
			'<div ' + 'id=' + '"product' + i + '" class="item row_nowrap">' +
			'<div class="col col2">' +
			'<a href="product.html" onmousedown="saveID(\'' + cart[i] + '\')"><img class="mobile_img" src="imgs/product' + cart[i] + '/1.png"></a></div>' +
			'<div class="col col2">' + '<h5 class="brand">' + products[cart[i]].brand + '</h5>' +
			'<div class="row"><a class="col21_1 product_name" href="product.html" onmousedown="saveID(\'' + cart[i] + '\')"><h4>' + products[cart[i]].product_name + '</h4></a>' +
			'<p class="col21_2 price">$' + products[cart[i]].price + '</p></div>' +
			'<div class="col"><p>Color: Black</p><p>Size: Medium</p></div>' +
			'<div class="edit_buttons"><a class="default_button" href="#">edit</a>' +
			'<a class="default_button remove"onclick="removeCart(\'' + cart[i] + '\');location.reload();">delete</a></div>' +
			'</div>';

		subtotal = parseFloat(subtotal) + parseFloat(products[cart[i]].price);
	}

	/** ORDER SUMMARY **/
	document.getElementById("subtotal").innerHTML = "$" + parseFloat(subtotal).toFixed(2);
	document.getElementById("tax").innerHTML = "$" + (parseFloat(subtotal) * 0.11).toFixed(2);
	document.getElementById("total").innerHTML = "$" + (parseFloat(subtotal) + (parseFloat(subtotal) * 0.11)).toFixed(2);
	document.getElementById("cart_number").innerHTML = cart.length + " items";
}