// Get the modal
var modal;


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal 
 function modalOpen(modalID) {
 	console.log("Modal Open: " + modalID);
  modal = document.getElementById('myModal'+ modalID);
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
function modalClose(modalID) {
  modal = document.getElementById('myModal'+ modalID);
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}



